import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DogComponent} from "./dog/dog.component";
import {DogFormComponent} from "./dog-form/dog-form.component";
import {ProfileComponent} from "../profile/profile/profile.component";


const routes: Routes = [
  {
    path: 'dog',
    component: DogComponent
  },
  {
    path: 'createDog',
    component: DogFormComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: '',
    component: DogComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
