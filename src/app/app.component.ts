import { Component } from '@angular/core';
import {Dog} from "./dog/model/Dog";
import {DOGS} from "./dog/data/dogs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'sda-angular-cl';
  dogs : Dog[] = DOGS;
  numOfDogs = this.dogs.length;
}
