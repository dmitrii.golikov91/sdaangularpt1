import {Dog} from "../model/Dog";

export const DOGS = [
  {
    name: "one",
    color: " black",
    age: 10,
    gender: "male",
  },

    {
    name: "two",
    color: "white",
    age: 40,
    gender: "female",
  },

    {
    name: "three",
    color:"green",
    age: 23,
    gender: "male",
  },
]
