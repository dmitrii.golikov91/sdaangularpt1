import {Component, Input, OnInit} from '@angular/core';
import {Dog} from "./model/Dog";
import {DOGS} from "./data/dogs";

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.less']
})
export class DogComponent implements OnInit {

 public doggos: Dog[];

  public someThing: boolean = false;
  constructor() {

  }

  ngOnInit(): void {
    this.fetchData();
setTimeout (() => this.someThing=!this.someThing, 3000)
  }

  onClick() {
  }

  fetchData() {
    this.doggos=DOGS;
  }

}
