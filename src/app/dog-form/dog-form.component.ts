import {Component, Input, OnInit} from '@angular/core';
import {Dog} from "../dog/model/Dog";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-dog-form',
  templateUrl: './dog-form.component.html',
  styleUrls: ['./dog-form.component.less']
})
export class DogFormComponent implements OnInit {

  @Input() dogs = []
  constructor() { }

  ngOnInit(): void {
  }

 async createDog (f: NgForm) {
    let dog = new Dog();
    dog.name = f.value.name;
    dog.color = f.value.color;
    dog.age= f.value.age;
    dog.gender = f.value.gender
      await this.dogs.push(dog)
  }

}
